/*
RocketChat-Terminal is a Terminal Client for RocketChatApp written in Node.js

Copyright (C) <2015>  <Peter Stevenson (2E0PGS)>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

var RocketChatApi = require('rocketchat').RocketChatApi; //core library for RocketChat
var multiline = require("multiline"); //used so we can have nice multiline /help responce
var configFile = require("./ConfigFile.json"); //stores users credentials

//--------------Variables--------------

var selectedChannel = "GENERAL";
var lastMessageSender;
var lastMessageChannel;
var lastMessagePMSender;
var lastMessagePMChannel;

//--------------Variables-----------END

var rocketChatApi = new RocketChatApi(configFile.protocol, configFile.host, configFile.port, configFile.user, configFile.password);

rocketChatApi.version(function(err,body){ //check version. If sucessful consider login sucessful. Print welcome message.
    if(err) {
		console.log(err);
	}
    else {
		console.log("########################################################");
		console.log("Version of API: " + body.versions.api + " Version of RocketChat: " + body.versions.rocketchat);
		console.log("For a list of commands type /commands");
		console.log("To join a channel do /channels and then do /join [channelname]");
		console.log("########################################################");
	}
})

function getNewMessages() {
	rocketChatApi.getUnreadMsg(selectedChannel ,function(err,body){
		if(err){
			console.log(err);
		}
		else {
			console.log("***New Messages***: ");
				for(var msg in body.messages) {
					console.log("Sender: " + body.messages[msg]._id + ", MSG: " + body.messages[msg].msg);
				}
		}		
	});
}

//------------Inputs-From-Terminal-Interface------------

process.stdin.setEncoding('utf8');

process.stdin.on('data', function (TerminalInputRAW) {

	var TerminalInput = TerminalInputRAW.replace(/(\r\n|\n|\r|\t)/gm,""); //trim the \r\n off the end of string
	var TerminalInput_array = TerminalInput.split(" ", 1); //take the input string and converts to array.
	var TerminalInput_Prefix = TerminalInput_array.toString(); //takes array and convers to string.
	var TerminalInput_Suffix = TerminalInput.substring(TerminalInput_Prefix.length + 1); //takes suffix of input string.
	
	/*
	if (TerminalInput === '/n') {
		console.log(myClient.users.length)
	}
	*/
	if (TerminalInput === '/commands' || TerminalInput === '/help') {
		console.log(commands);
	}
	else if (TerminalInput === '/quit') {
		console.log("Quitting RocketChat-Terminal");
		process.exit();
	}
	else if (TerminalInput === '/channels') {
		rocketChatApi.getPublicRooms(function(err,body){
			if(err) {
				console.log(err);
			}
			else {
				console.log("Rooms avaliable: ");
				for(var name in body.rooms) {
					console.log("Room: " + name + ", name: " + body.rooms[name].name + ", ID : " + body.rooms[name]._id);
				}
			}
		});
	}
	else if (TerminalInput_Prefix === '/join') {
		selectedChannel = TerminalInput_Suffix;
		console.log("Joined channel: " + selectedChannel)
	}
	/*
	else if (TerminalInput_Prefix === '/nick') {
		var New_Nick = TerminalInput_Suffix;
		myClient.setUsername(New_Nick);
		console.log("Changed Nick to: " + New_Nick);
	}	
	else if (TerminalInput_Prefix === '/setgame') {
		var New_Game = TerminalInput_Suffix;
		myClient.setPlayingGame(New_Game);
		console.log("Changed Playing to: " + New_Game);
	}
	else if (TerminalInput === '/idle') {
		myClient.setStatusIdle();
	}
	else if (TerminalInput === '/online') {
		myClient.setStatusOnline();
	}
	else if (TerminalInput_Prefix === '/reply') {
		//send a reply to last channel message
		myClient.sendMessage(lastMessageChannel, lastMessageSender + " " + TerminalInput_Suffix);
	}	
	else if (TerminalInput_Prefix === '/replypm') {
		//send a reply to last Private Message
		myClient.sendMessage(lastMessagePMChannel, lastMessagePMSender + " " + TerminalInput_Suffix);
	}
	*/
	else if (TerminalInput_Prefix === '/me') {
		//We have to add /me as it would otherwise be blocked by "/" filter as it looks like a command.		
		rocketChatApi.sendMsg(selectedChannel, "*" + TerminalInput_Suffix + "*",function(err,body){
			if(err) {
				console.log(err);
			}			
		});	
	}
	else if (TerminalInput_Prefix === '/tableflip') {
		//adding tableflip. This is a desktop client command.
		rocketChatApi.sendMsg(selectedChannel, "(╯°□°）╯︵ ┻━┻",function(err,body){
			if(err) {
				console.log(err);
			}			
		});		
	}
	/*
	else if (TerminalInput_Prefix === '/tts') {
		//Send TTS message.
		myClient.sendMessage(selectedChannel, TerminalInput_Suffix, {tts:true});
	}
	*/
	else if (TerminalInput_Prefix === '/clear') {
		//clears the terminal. Tested on Linux & Windows.
		console.log('\033[2J');
	}
	else if (TerminalInput_Prefix === '/getmsg') {
		getNewMessages();
	}
	else if (TerminalInput[0] != '/' && TerminalInput !== "") { //if we are not using a command or sending a blank message.
		//console.log("treating as message");
		rocketChatApi.sendMsg(selectedChannel, TerminalInput,function(err,body){
			if(err) {
				console.log(err);
			}			
		});
	}
});

//------------Inputs-From-Terminal-Interface---------END





var commands = multiline(function() {/*
Commands: /n lists online users in current channel. /channels lists avaliable channels. /join [channelname] to change channel. /nick [nickname] to change your username.
/tts [message] sends tts message. /reply [message] mentions last sender. /replypm replies to last PM. /clear clears the terminal window. /tableflip sends tableflip.
/online sets status online. /idle sets status idle/away. /quit quits RocketChat-Terminal. /setgame [game] changes playing status.
*/});

var about = multiline(function() {/*
About:-----------------------------------------------
RocketChat-Terminal written by: Peter Stevenson (2E0PGS)
Version: 1.0.0.0
Licence: GPL Version 3
-----------------------------------------------------
*/});
