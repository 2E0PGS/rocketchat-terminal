# RocketChat-Terminal #

## Terminal client for RocketChat ##

RocketChat-Terminal allows you to access RocketChat and enables you to chat in your favourite channels using a Terminal. This makes RocketChat behave in a IRC like fashion. I have based a few commands around IRSSI a IRC client for Linux and Mac.  

### How do I get set up? ###

* Clone Repo (master branch)
* Edit ConfigFile_EXAMPLE.json and add credentials. Then save it as ConfigFile.json
* apt get install node.js or install .exe
* npm update in the source folder. 
* ./"start RocketChat-Terminal Linux.sh"

### Licence ###

```
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
```